/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import ua.com.codefire.dao.models.Contact;

/**
 *
 * @author CodeFire
 */
public class ContactDaoJpaImpl implements ContactDao {
    
    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("PhonebookAppPU");

    private EntityManager getConnection() {
        return factory.createEntityManager();
    }

    @Override
    public List<Contact> getAll() {
        return getConnection().createQuery("from Contact c", Contact.class).getResultList();
    }

    @Override
    public Contact delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(Contact contact) {
        EntityManager manager = getConnection();
        manager.getTransaction().begin();
        manager.persist(contact);
        manager.getTransaction().commit();
    }

}
