/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.dao;

import java.util.ArrayList;
import java.util.List;
import ua.com.codefire.dao.models.Contact;

/**
 * DAO (Data Access Object)
 * @author CodeFire
 */
public class ContactMockImpl implements ContactDao{
    
    private static int id;
    
    private final List<Contact> list = new ArrayList<>();
    
    {
        list.add(new Contact(++id, "Ivan", "Ivanov", "123-45-67", "ivan@gmail.com"));
        list.add(new Contact(++id, "Petr", "Petrov", "111-22-33", "petr@gmail.com"));
        list.add(new Contact(++id, "Sidor", "Sidorov", "000-00-00", "sidor@gmail.com"));
    }

    @Override
    public List<Contact> getAll() {
        return new ArrayList<>(list);
    }

    @Override
    public Contact delete(int id) {
        for (int i = 0; i < list.size(); i++) {
            Contact current = list.get(i);
            if(current.getId() == id) {
                return list.remove(i);
            }
        }
        return null;
    }

    @Override
    public void add(Contact contact) {
        contact.setId(++id);
        list.add(contact);
    }
    
}
