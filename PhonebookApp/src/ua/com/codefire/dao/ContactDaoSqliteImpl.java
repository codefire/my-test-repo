/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ua.com.codefire.dao.models.Contact;

/**
 *
 * @author CodeFire
 */
public class ContactDaoSqliteImpl implements ContactDao {

    private static final String DATABASE = "database.sqlite";

    private static final String SELECT_QUERY = "SELECT * FROM contacts";
    private static final String INSERT_QUERY_STUB = "INSERT INTO contacts "
            + "(first_name, second_name, phone, email) "
            + "VALUES ('%s', '%s', '%s', '%s')";

    static {
        try {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContactDaoSqliteImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:" + DATABASE);
    }

    @Override
    public List<Contact> getAll() {
        List<Contact> list = new ArrayList<>();
        try (Connection connect = getConnection()) {
            Statement state = connect.createStatement();
            ResultSet result = state.executeQuery(SELECT_QUERY);
            while (result.next()) {
                list.add(new Contact(
                        result.getInt("id"),
                        result.getString("first_name"),
                        result.getString("second_name"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ContactDaoSqliteImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Contact delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(Contact contact) {
        //String message = String.format("Hello! My name is %s. My age is %d.", "John", 25);
        // message -> "Hello! My name is John. My age is 25."
        String query = String.format(
                INSERT_QUERY_STUB,
                contact.getFirstName(), contact.getSecondName(), contact.getPhone(), contact.getEmail());
        try (Connection connect = getConnection()) {
            Statement state = connect.createStatement();
            state.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(ContactDaoSqliteImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
